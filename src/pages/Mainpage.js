import React, { useState, useContext } from "react";
import "../App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { CartContext } from "../context/cartContext";
import { Button, Dropdown } from "react-bootstrap";

import { FaChevronLeft } from "react-icons/fa";

export default function Mainpage() {
  const [state, dispatch] = useContext(CartContext);

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <a
      href=""
      style={{ color: "#007bff00" }}
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
    >
      {children}
      &#x25bc;
    </a>
  ));

  return (
    <div>
      <div
        className="card-header"
        style={{
          height: "60%",
          width: "98%",
          backgroundColor: "#E6F2FD",
          marginTop: "3%",
          borderRadius: "10px",
        }}
      >
        <div className="container">
          <div className="row">
            <div className="col col-md-8">
              <div style={{ marginLeft: "8%", marginTop: "10%" }}>
                <p className="namabannerbesar">
                  Share, read, and <i>love</i>
                </p>
                <p style={{ marginTop: "10%" }} className="namabannerkecil">
                  Reading is Facinating
                </p>
              </div>
            </div>
            <div className="col col-md-4">
              <div>
                <img
                  className="gamabrbukubanner"
                  src={require("../images/bookfix.png")}
                />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="row" style={{ width: "102.5%" }}>
          <div className="col col-md-6">
            <p className="listBook">List Buku</p>
          </div>
          <div
            className="col col-md-6"
            style={{ textAlign: "right", paddingRight: "0" }}
          >
            <Dropdown>
              <Dropdown.Toggle
                as={CustomToggle}
                id="dropdown-custom-components"
              >
                <Button
                  style={{
                    backgroundColor: "rgba(233, 233, 233, 0.7)",
                    borderColor: "rgba(233, 233, 233, 0.7)",
                    color: "black",
                    marginTop: "3%",
                  }}
                >
                  <div className="buttoncategory">
                    <FaChevronLeft />
                    <p style={{ display: "unset" }}>Category</p>
                  </div>
                </Button>
              </Dropdown.Toggle>

              <Dropdown.Menu>
                <Dropdown.Item>
                  {" "}
                  <p> Sci-fy</p>
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>
        </div>
        <div className="row">
          <div className="col col-md-3">
            <div>
              <img
                className="gambarbuku"
                src={require("../images/Rectangle9.png")}
              />
              <p className="judulbuku">What if? Absurb Question</p>
              <p className="pengarang">Randall Munroe</p>
            </div>
          </div>

          <div className="col col-md-3">
            <div>
              <img
                className="gambarbuku"
                src={require("../images/Rectangle9.png")}
              />
              <p className="judulbuku">What if? Absurb Question</p>
              <p className="pengarang">Randall Munroe</p>
            </div>
          </div>

          <div className="col col-md-3">
            <div>
              <img
                className="gambarbuku"
                src={require("../images/Rectangle9.png")}
              />
              <p className="judulbuku">What if? Absurb Question</p>
              <p className="pengarang">Randall Munroe</p>
            </div>
          </div>

          <div className="col col-md-3">
            <div>
              <img
                className="gambarbuku"
                src={require("../images/Rectangle9.png")}
              />
              <p className="judulbuku">What if? Absurb Question</p>
              <p className="pengarang">Randall Munroe</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
