import React, { Component, useState } from "react";
import ReactDOM from "react-dom";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Modal, Form } from "react-bootstrap";
import { BrowserRouter, Switch, Link, Route, Redirect } from "react-router-dom";
import PrivateRoute from "./components/PrivateRoute";
import { CartContextProvider } from "./context/cartContext";
import { FaUser } from "react-icons/fa";
import { FaBook } from "react-icons/fa";
import { FaBookMedical } from "react-icons/fa";
import { FaSignOutAlt } from "react-icons/fa";

// Import Page
import Mainpage from "./pages/Mainpage";
import LandingPage from "./pages/LandingPage";
import Profile from "./pages/Profile";
import Library from "./pages/Library";
import DetailBook from "./pages/DetailBook";
import AddBook from "./pages/AddBook";
import AdminHome from "./admin/Home";
import Reader from "./components/Readerpdf";

//Import Component
import Navbar from "./components/Navbar";

export default function App() {
  return (
    // <div>
    //   {/* <AdminHome /> */}
    //   <LandingPage />

    // </div>

    <CartContextProvider>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={LandingPage} />
          <PrivateRoute exact path="/admin" component={AdminHome} />
          <PrivateRoute exact path="/pdf/:id" component={Reader} />
          <div
            className="Container"
            style={{ marginLeft: "0", marginRight: "0" }}
          >
            <div className="row">
              <div className="col col-md-3" style={{ textAlign: "center" }}>
                <Navbar />
              </div>
              <div className="col col-md-9">
                <PrivateRoute exact path="/main" component={Mainpage} />
                <PrivateRoute exact path="/profile" component={Profile} />
                <PrivateRoute exact path="/library" component={Library} />
                <PrivateRoute exact path="/detail/:id" component={DetailBook} />
                <PrivateRoute exact path="/add" component={AddBook} />
              </div>
            </div>
          </div>
        </Switch>
      </BrowserRouter>
    </CartContextProvider>
  );
}
