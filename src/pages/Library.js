import React, {useState, useContext} from 'react';
import { CartContext } from "../context/cartContext";
import { Link, useHistory } from "react-router-dom";
import { book } from "../data/Book";

export default function Library() {
    const history = useHistory();
    const [state, dispatch] = useContext(CartContext);
    return (
        <div>
            <div className="container">
        <p className="listBook">My Library</p>
        <div className="row">
        
        {book.map((book) => (
                
                    <div className="col col-md-3">
                        <Link onClick={() => history.push(`/detail/${book.id}`)}>
                        <div>
                        <img className="gambarbuku" src={require('../images/Rectangle9.png')} />
                        <p className="judulbuku">{book.title}</p>
                        <p className="pengarang">{book.author}</p>
                        </div>
                        </Link>
                    </div>
                
        ))}

        </div>
        </div>
        </div>
    )
}
