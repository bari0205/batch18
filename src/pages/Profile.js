import React, {useState, useContext} from 'react';
import '../App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, Modal, Form } from 'react-bootstrap';
import { CartContext } from "../context/cartContext";

import { FaUser } from 'react-icons/fa';
import { FaBook } from 'react-icons/fa';
import { FaBookMedical } from 'react-icons/fa';
import { FaSignOutAlt } from 'react-icons/fa';
import { FaTransgender } from 'react-icons/fa';
import { FaRegAddressBook } from 'react-icons/fa';
import { FaMapMarkerAlt } from 'react-icons/fa';


export default function Profile() {
  const [state, dispatch] = useContext(CartContext);
    return (
        <div>
          <p className="listBook">Profile</p>
             <div className="card-header" style={{height:"60%",width:"98%",backgroundColor:"#FDEDE6", marginTop:"3%", borderRadius:"10px"}}>
                <div className="container">
                  <div className="row">
                    <div className="col col-md-8">
                      <div style={{marginLeft:"8%", marginTop:"3%"}}>
                      <div>
                          <div style={{display:"inline"}}><FaUser color="#8A8C90" size={20}/></div>
                          <p className="judulprofile" style={{display:"inline"}}>egigans@gmail.com</p> 
                          <p className="subjudulprofile" style={{display:"block"}}>Email</p> 
                      </div>
                      
                      <div>
                          <div style={{display:"inline"}}><FaTransgender color="#8A8C90" size={20}/></div>
                          <p className="judulprofile" style={{display:"inline"}}>Male</p> 
                          <p className="subjudulprofile" style={{display:"block"}}>Gender</p> 
                      </div>

                      <div>
                          <div style={{display:"inline"}}><FaRegAddressBook color="#8A8C90" size={20}/></div>
                          <p className="judulprofile" style={{display:"inline"}}>081286238911</p> 
                          <p className="subjudulprofile" style={{display:"block"}}>Contact</p> 
                      </div>

                      <div>
                          <div style={{display:"inline"}}><FaMapMarkerAlt color="#8A8C90" size={20}/></div>
                          <p className="judulprofile" style={{display:"inline"}}>Perumahan Permata Bintaro Residence C-3</p> 
                          <p className="subjudulprofile" style={{display:"block"}}>Address</p> 
                      </div>

                      </div>
                    </div>
                    <div className="col col-md-3">
                      <div>
                        <img className="gambarprofile" src={require('../images/profile.png')} />
                      </div>
                      <div>
                          <button className="btn gantipro" style={{backgroundColor:"#EE4622", color:"White"}}> 
                          <font className="tulisanganti">Change Profile</font>
                          </button>
                      </div>
                    </div>
                  </div>
                </div>

  
              </div>
              <div className="container" style={{paddingLeft:"0px"}}>
        <p className="listBook">My Books</p>
        <div className="row">
            <div className="col col-md-3">
                <div>
                <img className="gambarbuku" src={require('../images/Rectangle9.png')} />
                <p className="judulbuku">What if? Absurb Question</p>
                <p className="pengarang">Randall Munroe</p>
                </div>
            </div>

            <div className="col col-md-3">
                <div>
                <img className="gambarbuku" src={require('../images/Rectangle9.png')} />
                <p className="judulbuku">What if? Absurb Question</p>
                <p className="pengarang">Randall Munroe</p>
                </div>
            </div>
            
        </div>
        </div>
        </div>
    )
}
