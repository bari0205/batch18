import React, { useState, useContext } from "react";
import "../App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Modal, Form } from "react-bootstrap";
import {
  BrowserRouter as Router,
  Switch,
  Link,
  Route,
  Redirect,
  useHistory,
} from "react-router-dom";
import { CartContext } from "../context/cartContext";

import { FaUser } from "react-icons/fa";
import { FaBook } from "react-icons/fa";
import { FaBookMedical } from "react-icons/fa";
import { FaSignOutAlt } from "react-icons/fa";

export default function Navbar() {
  const [state, dispatch] = useContext(CartContext);
  const history = useHistory();
  return (
    <div>
      <div>
        <Link to="/main">
          <img
            src={require("../images/Icon.png")}
            style={{ marginLeft: "-10%", marginTop: "9%", width: "60%" }}
          ></img>
        </Link>
      </div>
      <div>
        <img
          className="shadowelips"
          src={require("../images/Ellipse1.png")}
          style={{ marginTop: "10%", width: "30%" }}
        ></img>
      </div>
      <div className="eginame">
        <font>Egi Ganteng</font>
      </div>
      <div style={{ marginLeft: "20%" }}>
        <hr
          style={{
            height: "0.5px",
            backgroundColor: "#C9C9C9",
            marginTop: "10%",
          }}
        />
      </div>
      <div style={{ marginLeft: "20%", textAlign: "left" }}>
        <Link className="linkmenu" style={{ color: "black" }} to="/profile">
          <button className="buttonmenu">
            <div style={{ marginLeft: "5%" }}>
              <FaUser size={24} /> <font className="namamenu"> Profile </font>
            </div>
          </button>
        </Link>

        <Link className="linkmenu" style={{ color: "black" }} to="/library">
          <button className="buttonmenu">
            <div style={{ marginLeft: "5%" }}>
              <FaBook size={24} />{" "}
              <font className="namamenu"> My Library </font>
            </div>
          </button>
        </Link>

        <Link className="linkmenu" style={{ color: "black" }} to="/add">
          <button className="buttonmenu">
            <div style={{ marginLeft: "5%" }}>
              <FaBookMedical size={24} />{" "}
              <font className="namamenu"> Add Book </font>
            </div>
          </button>
        </Link>
      </div>
      <div style={{ marginTop: "20%", marginLeft: "20%" }}>
        <hr
          style={{
            height: "0.5px",
            backgroundColor: "#C9C9C9",
            marginTop: "10%",
          }}
        />
      </div>
      <div style={{ marginLeft: "20%", textAlign: "left" }}>
        <Link className="linkmenu" style={{ color: "black" }}>
          <button
            className="buttonmenu"
            onClick={() =>
              dispatch({
                type: "LOGOUT",
              })
            }
          >
            <div style={{ marginLeft: "5%" }}>
              <FaSignOutAlt size={24} />{" "}
              <font className="namamenu"> Logout </font>
            </div>
          </button>
        </Link>
      </div>
    </div>
  );
}
