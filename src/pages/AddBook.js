import React, { useContext, useState } from "react";
import "../App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Modal, Form } from "react-bootstrap";
import { FaBookMedical } from "react-icons/fa";
import { CartContext } from "../context/cartContext";

export default function AddBook() {
  const [modalAdd, setModalAdd] = useState(false);
  const [state, dispatch] = useContext(CartContext);

  const [formData, setFormData] = useState({
    title: "",
    pub: "",
    name: "",
    cat: "",
    page: "",
    isbn: "",
  });

  const { title, pub, name, cat, page, isbn } = formData;

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
  };

  return (
    <div>
      <div>
        <p className="addbook">Add Book</p>
      </div>

      <div style={{ width: "95%", marginTop: "5%" }}>
        <Form onSubmit={(e) => handleSubmit(e)}>
          <Form.Group controlId="">
            <Form.Control
              type="text"
              placeholder="Title"
              name="title"
              value={title}
              onChange={(e) => handleChange(e)}
            />
          </Form.Group>
          <Form.Group controlId="">
            <Form.Label>Publication Date</Form.Label>
            <Form.Control
              type="date"
              placeholder="Publication Date"
              name="pub"
              value={pub}
              onChange={(e) => handleChange(e)}
            />
          </Form.Group>
          <Form.Group controlId="">
            <Form.Control
              type="text"
              placeholder="Full Name"
              name="name"
              value={name}
              onChange={(e) => handleChange(e)}
            />
          </Form.Group>
          <Form.Group controlId="">
            <Form.Control
              type="text"
              placeholder="Category"
              name="cat"
              value={cat}
              onChange={(e) => handleChange(e)}
            />
          </Form.Group>
          <Form.Group controlId="">
            <Form.Control
              type="text"
              placeholder="Pages"
              name="page"
              value={page}
              onChange={(e) => handleChange(e)}
            />
          </Form.Group>
          <Form.Group controlId="">
            <Form.Control
              type="text"
              placeholder="ISBN"
              name="isbn"
              value={isbn}
              onChange={(e) => handleChange(e)}
            />
          </Form.Group>

          <Form.Group controlId="">
            <Form.Control
              as="textarea"
              placeholder="Address"
              style={{ height: "150px" }}
              name="add"
            />
          </Form.Group>

          <Form.Group controlId="">
            <Form.Control type="file" placeholder="Attache File" />
          </Form.Group>
          <div style={{ float: "right" }}>
            <Button
              type="submit"
              variant=""
              style={{ backgroundColor: "#EE4622" }}
              onClick={() => setModalAdd(true)}
            >
              <font className="namabutton">
                Add Book <FaBookMedical />
              </font>
            </Button>
          </div>
        </Form>
      </div>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>

      <Modal
        size="lg"
        show={modalAdd}
        onHide={() => setModalAdd(false)}
        dialogClassName="modal-90w besardialog"
        contentClassName="besardialog"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header
          closeButton
          style={{ borderBottom: "white" }}
        ></Modal.Header>
        <Modal.Body>
          <div className="ketmod">
            <p style={{ width: "100%" }}>
              Thank you for adding your own books to our website, please wait 1
              x 24 hours to verify whether this book is your writing
            </p>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
}
