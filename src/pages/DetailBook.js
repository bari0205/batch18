import React, { useState, useContext } from "react";
import { Button, Modal, Form } from "react-bootstrap";
import { useParams, useHistory } from "react-router-dom";
import { book } from "../data/Book";

import { FaChevronLeft, FaChevronRight } from "react-icons/fa";
import { FaRegBookmark } from "react-icons/fa";
import { CartContext } from "../context/cartContext";

export default function DetailBook() {
  const [modalAdd, setModalAdd] = useState(false);
  const history = useHistory();
  const [state, dispatch] = useContext(CartContext);
  const [modalRead, setModalRead] = useState(false);

  const { id } = useParams();
  const databook = book.filter((item) => item.id == id);
  const bookid = id - 1;
  return (
    <div>
      <div className="Container">
        <div className="row">
          <div className="col col-md-5" style={{ paddingRight: "0" }}>
            <img
              className="gambardetail"
              src={require("../images/bigbook.png")}
            />
          </div>

          <div className="col col-md-7">
            <div className="detailnya">
              <div>
                <p className="juduldetail">{book[bookid].title}</p>
                <p className="subjuduldetail">{book[bookid].author}</p>
              </div>

              <div style={{ marginTop: "8%" }}>
                <p className="isidetail">Publication</p>
                <p className="subisidetail">April 2020</p>
              </div>

              <div style={{ marginTop: "5%" }}>
                <p className="isidetail">Category</p>
                <p className="subisidetail">Sci-fy</p>
              </div>

              <div style={{ marginTop: "5%" }}>
                <p className="isidetail">Pages</p>
                <p className="subisidetail">436</p>
              </div>

              <div style={{ marginTop: "5%" }}>
                <p className="isidetail" style={{ color: "#EE4622" }}>
                  ISBN
                </p>
                <p className="subisidetail">9781789807554</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div style={{ marginLeft: "0%", width: "80%" }}>
        <hr style={{ height: "0.5px", backgroundColor: "#C9C9C9" }} />
      </div>
      <div className="container">
        <div className="row">
          <div style={{ marginTop: "3%" }}>
            <p className="aboutdetail">About This Book</p>
          </div>

          <div style={{ width: "81%" }}>
            <p className="subaboutdetail">
              In the medieval kingdom of Goredd, women are expected to be
              ladies, men are their protectors, and dragons get to be whomever
              they want. Tess, stubbornly, is a troublemaker. You can’t make a
              scene at your sister’s wedding and break a relative’s nose with
              one punch (no matter how pompous he is) and not suffer the
              consequences. As her family plans to send her to a nunnery, Tess
              yanks on her boots and sets out on a journey across the
              Southlands, alone and pretending to be a boy. Where Tess is headed
              is a mystery, even to her. So when she runs into an old friend,
              it’s a stroke of luck. This friend is a quigutl—a subspecies of
              dragon—who gives her both a purpose and protection on the road.
              But Tess is guarding a troubling secret. Her tumultuous past is a
              heavy burden to carry, and the memories she’s tried to forget
              threaten to expose her to the world in more ways than one.
              Returning to the fascinating world she created in the
              award-winning and New York Times bestselling Seraphina, Rachel
              Hartman introduces readers to a new character and a new quest,
              pushing the boundaries of genre once again in this wholly original
              fantasy.
            </p>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row" style={{ float: "right", width: "40%" }}>
          <div>
            <button
              onClick={() => setModalAdd(true)}
              className="btn"
              style={{
                color: "white",
                backgroundColor: "#EE4622",
                marginLeft: "-30%",
              }}
            >
              Add Library <FaRegBookmark />
            </button>
            <button
              className="btn"
              style={{
                color: "black",
                backgroundColor: "rgba(233, 233, 233, 0.7)",
                marginLeft: "10%",
              }}
              onClick={() => history.push(`/pdf/${book[bookid].id}`)}
            >
              Read Book <FaChevronRight />
            </button>
          </div>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
        </div>
      </div>

      <Modal
        size="lg"
        show={modalAdd}
        onHide={() => setModalAdd(false)}
        dialogClassName="modal-90w besardialog"
        contentClassName="besardialog"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header
          closeButton
          style={{ borderBottom: "white" }}
        ></Modal.Header>
        <Modal.Body>
          <div className="ketmod">
            <p style={{ width: "100%" }}>
              Your book has been added successfully
            </p>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
}
