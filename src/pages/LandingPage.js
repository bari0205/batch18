import React, { Component, useState, useContext, useh} from 'react';
import ReactDOM from 'react-dom';
import '../App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, Modal, Form } from 'react-bootstrap';
import {
  BrowserRouter as Router,
  Switch,
  Link,
  Route,
  Redirect,
  useHistory,
} from "react-router-dom";
import { CartContext } from "../context/cartContext";

// Import Page
import Mainpage from './Mainpage';

const inputform = {
  bginput : {
    backgroundColor: "rgba(210, 210, 210, 0.25)",
    height: "50px"
  }
};

const buttonpunya = {
  bgbutton : {
    width: "350px",
    height: "50px",
    left: "33px",
    top: "555px",
    background: "#EE4622",
    borderRadius: "5px",
    borderColor:"#EE4622"
  }
};

const noacc = {
  bentuk: {
    width: "276px",
    height: "25px",
    top: "346px",

    fontFamily: "Avenir",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "18px",
    lineHeight: "60px",
/* identical to box height */


color: "#333333"
  }
};

export default function App(){
  const history = useHistory();
  const [state, dispatch] = useContext(CartContext);
    const [modalSignup, setModalSignup] = useState(false);
    const [modalSignin, setModalSignin] = useState(false);

    const [formData, setFormData] = useState({
      email: "barii@gmail.com",
      password: "123456",
    });

    const { email, password } = formData;

    const handleChange = (e) => {
      setFormData({ ...formData, [e.target.name]: e.target.value });
    };

    const handleSubmit = (e) => {
      e.preventDefault();
      if (email === "barii@gmail.com" && password === "123456") {
        console.log("LOGIN SUCCESS");
        dispatch({
          type: "LOGIN",
        });
        history.push(`/main`);
        
      } 
      else if (email === "admin@gmail.com" && password === "123456") {
        console.log("LOGIN SUCCESS");
        dispatch({
          type: "LOGIN",
        });
        history.push(`/admin`);
        
      }
      else {
        console.log("LOGIN GAGAL");
      }
    };
    
    return(
      <div className="Container">

        <div className="row">
          
          <div className="col col-md-6" style={{paddingRight:"0px"}}>
          <img src={require('../images/Icon.png')} style={{margin:"6% 12%", width:"26%"}}></img>
          <div style={{width:"120%",marginLeft:"12%", marginTop:"5%", fontFamily:"Times New Roman", fontWeight:"bold", fontSize:"110px", lineHeight:"82.49%"}}><i>Your</i> library anywhere</div>
          <div style={{width:"68%",marginLeft:"12%", marginTop:"8%", lineHeight:"33px", fontSize:"24px"}}>Sign-up today and receive unlimited accesss to all of your reading - share your book.</div>
          <div style={{marginLeft:"12%", marginTop:"3%"}}>
            <div className="row">
            <div className="col col-md-5">
            <Button type="button" onClick={() => setModalSignup(true)} id="signup" className="btn" style={{ borderColor:"#EE4622",width:"100%", height:"50px",backgroundColor:"#EE4622", color:"white", fontFamily:"Avenir", fontSize:"18px"}}>Sign Up</Button>
            </div>
            <div className="col col-md-5">
            <Button onClick={() => setModalSignin(true)} className="btn" style={{width:"100%" , height:"50px", backgroundColor:"rgba(233, 233, 233, 0.7)", borderColor:"rgba(233, 233, 233, 0.7)", color:"black", fontFamily:"Avenir", fontSize:"18px"}}>Sign In</Button>
            </div>
            </div>            
          </div>
          </div>
          
          <div className="col col-md-6" style={{paddingLeft:"0px"}}>
            <img src={require('../images/Vector1.png')}></img>
          </div>
        
        </div>

        {/* Modal signup */}
        <Modal
        show={modalSignup}
        onHide={() => setModalSignup(false)}
      >
        <Modal.Header closeButton style={{borderBottom:"white"}}>
          {/* <Modal.Title id="example-modal-sizes-title-sm">
            Small Modal
          </Modal.Title> */}
          <div className="titlemodal">Sign Up</div>
        </Modal.Header>
        <Modal.Body style={{textAlign:"center"}}>
            <Form >
              <Form.Group controlId="">
                <Form.Control type="email" placeholder="Email" style={inputform.bginput}/>
              </Form.Group>
              <Form.Group controlId="formGroupPassword">
                <Form.Control type="password" placeholder="Password" style={inputform.bginput}/>
              </Form.Group>
              <Form.Group controlId="formGroupPassword">
                <Form.Control type="text" placeholder="Full Name" style={inputform.bginput}/>
              </Form.Group>
              <Form.Group controlId="formGroupPassword">
                <Form.Control as="select" style={inputform.bginput}>
                  <option value="">Gender</option>
                  <option value="man">Man</option>
                  <option value="women">Woman</option>
                </Form.Control>
              </Form.Group>
              <Form.Group controlId="formGroupPassword">
                <Form.Control type="number" placeholder="Phone" style={inputform.bginput}/>
              </Form.Group>
              <Form.Group controlId="formGroupPassword">
                <Form.Control as="textarea" placeholder="Address"style={inputform.bginput} />
              </Form.Group>
              <Button type="submit" variant="primary" style={buttonpunya.bgbutton}>Sign Up</Button>
            </Form>
        </Modal.Body>
        
      </Modal>
        
        {/* Modal signin */}
        <Modal
        show={modalSignin}
        onHide={() => setModalSignin(false)}
      >
        <Modal.Header closeButton style={{borderBottom:"white"}}>
          {/* <Modal.Title id="example-modal-sizes-title-sm">
            Small Modal
          </Modal.Title> */}
          <div className="titlemodal">Sign In</div>
        </Modal.Header>
        <Modal.Body style={{textAlign:"center"}}>
            <Form onSubmit={(e) => handleSubmit(e)}>
              <Form.Group controlId="">
                <Form.Control 
                  type="email"
                  name="email" 
                  placeholder="Email" 
                  style={inputform.bginput}
                  value={email}
                  onChange={(e) => handleChange(e)}
                />
              </Form.Group>
              <Form.Group controlId="formGroupPassword">
                <Form.Control 
                  type="password"
                  name="password" 
                  placeholder="Password" 
                  style={inputform.bginput}
                  value={password}
                  onChange={(e) => handleChange(e)}
                />
              </Form.Group>
              <Button type="submit" variant="primary" style={buttonpunya.bgbutton}>Sign In</Button>
            </Form>
            <br></br>
          <br></br>
          <font style={noacc.bentuk} >Don't Have Account ? Click <b>Here</b></font>
        </Modal.Body>
        
      </Modal>
        
    
      </div>
      
    )
}
